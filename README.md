# Dictionary Model

[![pipeline status](https://gitlab.com/Zalmoksis/dictionary-model/badges/master/pipeline.svg)](https://gitlab.com/Zalmoksis/dictionary-model/-/commits/master)
[![coverage report](https://gitlab.com/Zalmoksis/dictionary-model/badges/master/coverage.svg)](https://gitlab.com/Zalmoksis/dictionary-model/-/commits/master)

This repository contains a PHP model for dictionary data.
