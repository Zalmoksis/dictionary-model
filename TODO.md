# To do

## Bugfixes (and refactoring)

## Minor
- Common naming mechanism for nodes and node collections
- Deep cloning
- Subnodes stored in arrays not to have to update all dependencies all the time;
  generic `Node::has()`; generic elastic constructors, no setters.

## Major
