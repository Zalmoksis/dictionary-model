# Change log

All notable changes to this project will be documented in this file
in a format adhering to [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

The project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0).

## [Unreleased]
### Added
- CI pipeline for PHP 8.1

## [0.20.8] — 2020-12-14
### Added
- Node constructors
### Deprecated
- Subnode setters

## [0.20.7] — 2020-12-14
## Added
- Badges

## [0.20.6] — 2020-12-13
## Added
- Static analysis with Psalm
## Fixed
- Added some return types
- `Headwords::getFirst` now returns `null` when empty

## [0.20.5] — 2020-12-13
### Changed
- Fixed name of `Node`

## [0.20.4] — 2020-12-13
## Fixed
- Misconfigured coding standard pipeline

## [0.20.3] — 2020-12-12
### Fixed
- Fixed PHPUnit config

## [0.20.2] — 2020-12-12
### Changed
- Various configuration improvements

## [0.20.1] — 2020-12-09
### Added
- Extended todo list

## [0.20.0] — 2020-12-09
### Added
- Allowing Data Structures 0.4

## [0.19.2] — 2020-12-07
### Added
- Allowing PHP 8.0

## [0.19.1] — 2020-01-16
### Fixed
- Missing interface `NodeWithSoundChanges` on `Entry`

## [0.19.0] — 2020-01-15
### Added
- `Entry` can contain `SoundChanges` collection of `SoundChange` values.
- Strict types
### Removed
- Deprecated methods: `add` on collections and `addXXX` on some nodes
### Fixed
- Incorrect namespaces in unit tests

## [0.18.0] — 2019-12-17
### Added
- Labels: `Variety`, `Register`, `Domain`
### Changed
- Homograph index is a value object now as `HomographIndex`
### Deprecated
- `add` methods on collections

## [0.17.5] — 2019-12-16
### Added
- Partial interfaces now include setters.

## [0.17.4] — 2019-12-15
### Changed
- Re-enabled coding style test but with failure allowed

## [0.17.3] — 2019-12-15
### Changed
- Deprecated methods removed from tests

## [0.17.2] — 2019-12-15
### Added
- Partial interfaces
### Deprecated
- `add` methods on nodes

## [0.17.1] — 2019-12-10
### Fixed
- Typography in this changelog

## [0.17.0] — 2019-12-09
### Added
- References: `Synonym`, `Antonym`, `Derivative`
- Testing lowest dependencies in Gitlab CI

## [0.16.2] — 2019-12-03
### Changed
- Less noise in Gitlab CI

## [0.16.1] — 2019-11-30
### Changed
- Data structures (`Collection`) update
### Fixed
- Changelog dates

## [0.16.0] — 2019-11-29
### Changed
- Repository name to `dicitonary-model`

## [0.15.0] — 2019-11-29
### Added
- Coding standard enforcement
- Missing unit tests
- Pipeline
### Changed
- PHP upgrade do 7.4

## [0.14.0] — 2019-02-22
### Added
- Now a `Sense` can have a description attached as `Context`
### Changed
- `Etymon` and `Cognate` don't have constructor parameters anymore

## [0.13.1] — 2019-02-22
### Added
- Removing public keyword from methods

## [0.13.0] — 2019-02-18
### Added
- Etymology information can now be represented by `Etymon`-s and `Cognate`-s

## [0.12.1] — 2019-02-02
### Added
- "To do" list
### Fixed
- Missing item in change log

## [0.12.0] — 2019-02-02
### Added
- node names as a public constant for all nodes
- code coverage configuration for PHP Unit
- this change log
- MIT licence
### Removed
- `Storage` interface was removed. An equivalent will be held in a separate
  repository: [Zalmoksis\dictionary-storage](https://gitlab.com/Zalmoksis/dictionary-storage)
