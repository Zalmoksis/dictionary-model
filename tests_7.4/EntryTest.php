<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{
    Antonym,
    Category,
    Cognate,
    Collocation,
    Definition,
    Derivative,
    Domain,
    Entry,
    Etymon,
    Form,
    FormGroup,
    FormLabel,
    Gloss,
    Headword,
    HomographIndex,
    Language,
    Lemma,
    Node,
    Pronunciation,
    Register,
    Sense,
    SoundChange,
    Synonym,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Interfaces\{
    NodeWithAntonyms,
    NodeWithCategories,
    NodeWithCognates,
    NodeWithCollocations,
    NodeWithDefinition,
    NodeWithDerivatives,
    NodeWithEtymons,
    NodeWithFormNodes,
    NodeWithHeadwords,
    NodeWithPronunciations,
    NodeWithSenses,
    NodeWithSoundChanges,
    NodeWithSynonyms,
    NodeWithTranslations,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Cognates,
    Collocations,
    Derivatives,
    Domains,
    Etymons,
    FormNodes,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    SoundChanges,
    Synonyms,
    Translations,
    Varieties,
};

class EntryTest extends TestCase {
    protected Entry $entry;

    function setUp(): void {
        $this->entry = new Entry();
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->entry);
    }

    function testIfImplementsNodePartials(): void {
        $this->assertInstanceOf(NodeWithHeadwords::class, $this->entry);
        $this->assertInstanceOf(NodeWithPronunciations::class, $this->entry);
        $this->assertInstanceOf(NodeWithCategories::class, $this->entry);
        $this->assertInstanceOf(NodeWithFormNodes::class, $this->entry);
        $this->assertInstanceOf(NodeWithDefinition::class, $this->entry);
        $this->assertInstanceOf(NodeWithTranslations::class, $this->entry);
        $this->assertInstanceOf(NodeWithSynonyms::class, $this->entry);
        $this->assertInstanceOf(NodeWithAntonyms::class, $this->entry);
        $this->assertInstanceOf(NodeWithCollocations::class, $this->entry);
        $this->assertInstanceOf(NodeWithSenses::class, $this->entry);
        $this->assertInstanceOf(NodeWithDerivatives::class, $this->entry);
        $this->assertInstanceOf(NodeWithEtymons::class, $this->entry);
        $this->assertInstanceOf(NodeWithSoundChanges::class, $this->entry);
        $this->assertInstanceOf(NodeWithCognates::class, $this->entry);
    }

    function testNodeName(): void {
        $this->assertEquals('entry', $this->entry::NODE_NAME);
    }

    function testHeadwords(): void {
        $this->entry->setHeadwords(
            new Headwords(
                new Headword('headword 1'),
                new Headword('headword 2'),
            )
        );
        $this->assertEquals(
            new Headwords(
                new Headword('headword 1'),
                new Headword('headword 2'),
            ),
            $this->entry->getHeadwords()
        );
    }

    function testHomographIndex(): void {
        $this->assertNull($this->entry->getHomographIndex());

        $this->assertEquals(
            new HomographIndex(2),
            $this->entry->setHomographIndex(new HomographIndex(2))->getHomographIndex()
        );
    }

    function testPronunciations(): void {
        $this->entry->setPronunciations(
            new Pronunciations(
                new Pronunciation('pronunciation 1'),
                new Pronunciation('pronunciation 2'),
            )
        );
        $this->assertEquals(
            new Pronunciations(
                new Pronunciation('pronunciation 1'),
                new Pronunciation('pronunciation 2'),
            ),
            $this->entry->getPronunciations()
        );
    }

    function testCategories(): void {
        $this->entry->setCategories(
            new Categories(
                new Category('category 1'),
                new Category('category 2'),
            )
        );
        $this->assertEquals(
            new Categories(
                new Category('category 1'),
                new Category('category 2'),
            ),
            $this->entry->getCategories()
        );
    }

    function testFormNodes(): void {
        $formNodes = new FormNodes(
            (new Form(new FormLabel('label 1')))
                ->setHeadwords(new Headwords(new Headword('form 1'))),
            (new FormGroup(new FormLabel('label 2')))
                ->setFormNodes(
                    new FormNodes(
                        (new Form(new FormLabel('label 2.1')))
                            ->setHeadwords(new Headwords(new Headword('form 2.1')))
                    )
                )
        );
        $this->entry->setFormNodes($formNodes);
        $this->assertEquals($formNodes, $this->entry->getFormNodes());

        $this->entry->setFormNodes(
            new FormNodes(
                (new Form(new FormLabel('label 3')))
                    ->setHeadwords(new Headwords(new Headword('form 3')))
            ),
        );
        $this->entry->setFormNodes(
            new FormNodes(
                (new FormGroup(new FormLabel('label 4')))
                    ->setFormNodes(
                        new FormNodes(
                            (new Form(new FormLabel('label 4.1')))
                                ->setHeadwords(new Headwords(new Headword('form 4.1')))
                        )
                    )
            )
        );
    }

    function testVarieties(): void {
        $this->entry->setVarieties(
            new Varieties(
                new Variety('variety 1'),
                new Variety('variety 2'),
            )
        );

        $this->assertEquals(
            new Varieties(
                new Variety('variety 1'),
                new Variety('variety 2'),
            ),
            $this->entry->getVarieties()
        );
    }

    function testRegisters(): void {
        $this->entry->setRegisters(
            new Registers(
                new Register('register 1'),
                new Register('register 2'),
            )
        );

        $this->assertEquals(
            new Registers(
                new Register('register 1'),
                new Register('register 2'),
            ),
            $this->entry->getRegisters()
        );
    }

    function testDomains(): void {
        $this->entry->setDomains(
            new Domains(
                new Domain('domain 1'),
                new Domain('domain 2'),
            )
        );

        $this->assertEquals(
            new Domains(
                new Domain('domain 1'),
                new Domain('domain 2'),
            ),
            $this->entry->getDomains()
        );
    }

    function testDefinition(): void {
        $this->entry->setDefinition(new Definition('definition'));
        $this->assertEquals(
            new Definition('definition'),
            $this->entry->getDefinition(),
        );
    }

    function testTranslations(): void {
        $this->entry->setTranslations(
            new Translations(
                new Translation('translation 1'),
                new Translation('translation 2'),
            )
        );
        $this->assertEquals(
            new Translations(
                new Translation('translation 1'),
                new Translation('translation 2'),
            ),
            $this->entry->getTranslations()
        );
    }

    function testSynonyms(): void {
        $this->entry->setSynonyms(new Synonyms(
            new Synonym('synonym 1'),
            new Synonym('synonym 2'),
        ));

        $this->assertEquals(
            new Synonyms(
                new Synonym('synonym 1'),
                new Synonym('synonym 2'),
            ),
            $this->entry->getSynonyms()
        );
    }

    function testAntonyms(): void {
        $this->entry->setAntonyms(new Antonyms(
            new Antonym('antonym 1'),
            new Antonym('antonym 2'),
        ));

        $this->assertEquals(
            new Antonyms(
                new Antonym('antonym 1'),
                new Antonym('antonym 2'),
            ),
            $this->entry->getAntonyms()
        );
    }

    function testCollocations(): void {
        $this->entry
            ->setCollocations(new Collocations(
                (new Collocation())
                    ->setHeadwords(new Headwords(new Headword('headword 1')))
                    ->setTranslations(new Translations(new Translation('translation 1'))),
                (new Collocation())
                    ->setHeadwords(new Headwords(new Headword('headword 2')))
                    ->setTranslations(new Translations(new Translation('translation 2'))),
            ));
        $this->assertEquals(
            new Collocations(
                (new Collocation())
                    ->setHeadwords(new Headwords(new Headword('headword 1')))
                    ->setTranslations(new Translations(new Translation('translation 1'))),
                (new Collocation())
                    ->setHeadwords(new Headwords(new Headword('headword 2')))
                    ->setTranslations(new Translations(new Translation('translation 2'))),
            ),
            $this->entry->getCollocations()
        );
    }

    function testSenses(): void {
        $this->entry->setSenses(
            new Senses(
                (new Sense())->setTranslations(new Translations(new Translation('translation 1'))),
                (new Sense())->setTranslations(new Translations(new Translation('translation 2'))),
            )
        );
        $this->assertEquals(
            new Senses(
                (new Sense())->setTranslations(new Translations(new Translation('translation 1'))),
                (new Sense())->setTranslations(new Translations(new Translation('translation 2'))),
            ),
            $this->entry->getSenses()
        );
    }

    function testDerivatives(): void {
        $this->entry->setDerivatives(new Derivatives(
            new Derivative('derivative 1'),
            new Derivative('derivative 2'),
        ));

        $this->assertEquals(
            new Derivatives(
                new Derivative('derivative 1'),
                new Derivative('derivative 2'),
            ),
            $this->entry->getDerivatives()
        );
    }

    function testEtymons(): void {
        $this->entry->setEtymons(new Etymons(
            (new Etymon())
                ->setLanguage(new Language('language 1'))
                ->setLemma(new Lemma('lemma 1'))
                ->setGloss(new Gloss('gloss 1')),
            (new Etymon())
                ->setLanguage(new Language('language 2'))
                ->setLemma(new Lemma('lemma 2'))
                ->setGloss(new Gloss('gloss 2')),
        ));

        $this->assertEquals(
            new Etymons(
                (new Etymon())
                    ->setLanguage(new Language('language 1'))
                    ->setLemma(new Lemma('lemma 1'))
                    ->setGloss(new Gloss('gloss 1')),
                (new Etymon())
                    ->setLanguage(new Language('language 2'))
                    ->setLemma(new Lemma('lemma 2'))
                    ->setGloss(new Gloss('gloss 2')),
            ),
            $this->entry->getEtymons()
        );
    }

    function testSoundChanges(): void {
        $this->entry->setSoundChanges(new SoundChanges(
            new SoundChange('sound change 1'),
            new SoundChange('sound change 2'),
        ));

        $this->assertEquals(
            new SoundChanges(
                new SoundChange('sound change 1'),
                new SoundChange('sound change 2'),
            ),
            $this->entry->getSoundChanges()
        );
    }

    function testCognates(): void {
        $this->entry->setCognates(new Cognates(
            (new Cognate())
                ->setLanguage(new Language('language 1'))
                ->setLemma(new Lemma('lemma 1'))
                ->setGloss(new Gloss('gloss 1')),
            (new Cognate())
                ->setLanguage(new Language('language 2'))
                ->setLemma(new Lemma('lemma 2'))
                ->setGloss(new Gloss('gloss 2')),
        ));

        $this->assertEquals(
            new Cognates(
                (new Cognate())
                    ->setLanguage(new Language('language 1'))
                    ->setLemma(new Lemma('lemma 1'))
                    ->setGloss(new Gloss('gloss 1')),
                (new Cognate())
                    ->setLanguage(new Language('language 2'))
                    ->setLemma(new Lemma('lemma 2'))
                    ->setGloss(new Gloss('gloss 2')),
            ),
            $this->entry->getCognates()
        );
    }
}
