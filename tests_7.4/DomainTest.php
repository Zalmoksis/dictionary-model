<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Domain, Node, Value};

class DomainTest extends TestCase {
    protected Domain $domain;

    function setUp(): void {
        $this->domain = new Domain('some domain');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->domain);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->domain);
    }

    function testNodeName(): void {
        $this->assertEquals('domain', $this->domain::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('some domain', $this->domain->getValue());
    }
}
