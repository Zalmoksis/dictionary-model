<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Headword, Node, Value};

class HeadwordTest extends TestCase {
    protected Headword $headword;

    function setUp(): void {
        $this->headword = new Headword('some headword');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->headword);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->headword);
    }

    function testNodeName(): void {
        $this->assertEquals('headword', $this->headword::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('some headword', $this->headword->getValue());
    }
}
