<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Derivative, Node, Reference};

class DerivativeTest extends TestCase {
    protected Derivative $derivative;

    function setUp(): void {
        $this->derivative = new Derivative('someDerivative');
    }

    function testIfImplementsReference(): void {
        $this->assertInstanceOf(Reference::class, $this->derivative);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->derivative);
    }

    function testNodeName(): void {
        $this->assertEquals('derivative', $this->derivative::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('someDerivative', $this->derivative->getHeadword());
    }
}
