<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Collections\Headwords, Headword};

class HeadwordsTest extends TestCase {
    protected Headwords $headwords;

    function setUp(): void {
        $this->headwords = new Headwords(
            new Headword('headword 1'),
            new Headword('headword 2'),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->headwords);
    }

    function testCollectionName(): void {
        $this->assertEquals('headwords', $this->headwords::NODE_COLLECTION_NAME);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->headwords);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->headwords);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->headwords);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->headwords as $headword) {
            $elements[] = $headword;
        }

        $this->assertEquals([
            new Headword('headword 1'),
            new Headword('headword 2'),
        ], $elements);
    }

    function testGettingFirst(): void {
        $this->assertEquals(new Headword('headword 1'), $this->headwords->getFirst());
    }
}
