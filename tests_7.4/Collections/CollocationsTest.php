<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Collections\Collocations, Collections\Headwords, Collocation, Headword};

class CollocationsTest extends TestCase {
    protected Collocations $collocations;

    function setUp(): void {
        $this->collocations = new Collocations(
            (new Collocation())->setHeadwords(new Headwords(new Headword('collocation 1'))),
            (new Collocation())->setHeadwords(new Headwords(new Headword('collocation 2'))),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->collocations);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->collocations);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->collocations);
    }

    function testCollectionName(): void {
        $this->assertEquals('collocations', $this->collocations::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->collocations);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->collocations as $collocation) {
            $elements[] = $collocation;
        }

        $this->assertEquals([
            (new Collocation())->setHeadwords(new Headwords(new Headword('collocation 1'))),
            (new Collocation())->setHeadwords(new Headwords(new Headword('collocation 2'))),
        ], $elements);
    }
}
