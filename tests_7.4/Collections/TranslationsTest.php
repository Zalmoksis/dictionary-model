<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Collections\Translations, Translation};

class TranslationsTest extends TestCase {
    protected Translations $translations;

    function setUp(): void {
        $this->translations = new Translations(
            new Translation('translation 1'),
            new Translation('translation 2'),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->translations);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->translations);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->translations);
    }

    function testCollectionName(): void {
        $this->assertEquals('translations', $this->translations::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->translations);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->translations as $translation) {
            $elements[] = $translation;
        }

        $this->assertEquals([
            new Translation('translation 1'),
            new Translation('translation 2'),
        ], $elements);
    }
}
