<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Collections\Domains, Domain};

class DomainsTest extends TestCase {
    protected Domains $domains;

    function setUp(): void {
        $this->domains = new Domains(
            new Domain('domain 1'),
            new Domain('domain 2'),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->domains);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->domains);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->domains);
    }

    function testCollectionName(): void {
        $this->assertEquals('domains', $this->domains::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->domains);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->domains as $domain) {
            $elements[] = $domain;
        }

        $this->assertEquals([
            new Domain('domain 1'),
            new Domain('domain 2'),
        ], $elements);
    }
}
