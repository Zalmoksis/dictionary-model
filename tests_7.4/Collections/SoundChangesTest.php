<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Collections\SoundChanges, SoundChange};

class SoundChangesTest extends TestCase {
    protected SoundChanges $soundChanges;

    function setUp(): void {
        $this->soundChanges = new SoundChanges(
            new SoundChange('sound change 1'),
            new SoundChange('sound change 2'),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->soundChanges);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->soundChanges);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->soundChanges);
    }

    function testCollectionName(): void {
        $this->assertEquals('sound changes', $this->soundChanges::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->soundChanges);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->soundChanges as $pronunciation) {
            $elements[] = $pronunciation;
        }

        $this->assertEquals([
            new SoundChange('sound change 1'),
            new SoundChange('sound change 2'),
        ], $elements);
    }
}
