<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Etymon, Gloss, Language, Lemma, Node};

class EtymonTest extends TestCase {
    protected Etymon $etymon;

    function setUp(): void {
        $this->etymon = new Etymon();
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->etymon);
    }

    function testNodeName(): void {
        $this->assertEquals('etymon', $this->etymon::NODE_NAME);
    }

    function testLanguage(): void {
        $this->etymon->setLanguage(new Language('some language'));
        $this->assertEquals(new Language('some language'), $this->etymon->getLanguage());

        $this->etymon->setLanguage(new Language('another language'));
        $this->assertEquals(new Language('another language'), $this->etymon->getLanguage());
    }

    function testLemma(): void {
        $this->etymon->setLemma(new Lemma('some lemma'));
        $this->assertEquals(new Lemma('some lemma'), $this->etymon->getLemma());

        $this->etymon->setLemma(new Lemma('another lemma'));
        $this->assertEquals(new Lemma('another lemma'), $this->etymon->getLemma());
    }

    function testGloss(): void {
        $this->etymon->setGloss(new Gloss('some gloss'));
        $this->assertEquals(new Gloss('some gloss'), $this->etymon->getGloss());

        $this->etymon->setGloss(new Gloss('another gloss'));
        $this->assertEquals(new Gloss('another gloss'), $this->etymon->getGloss());
    }
}
