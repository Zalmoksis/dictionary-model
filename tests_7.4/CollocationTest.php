<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\Collections\{
    Domains,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    Translations,
    Varieties,
};
use Zalmoksis\Dictionary\Model\{
    Collocation,
    Definition,
    Domain,
    Headword,
    Node,
    Pronunciation,
    Register,
    Sense,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Interfaces\{
    NodeWithDefinition,
    NodeWithHeadwords,
    NodeWithPronunciations,
    NodeWithSenses,
    NodeWithTranslations,
};

class CollocationTest extends TestCase {
    protected Collocation $collocation;

    function setUp(): void {
        $this->collocation = new Collocation();
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->collocation);
    }

    function testIfImplementsNodePartials(): void {
        $this->assertInstanceOf(NodeWithHeadwords::class, $this->collocation);
        $this->assertInstanceOf(NodeWithPronunciations::class, $this->collocation);
        $this->assertInstanceOf(NodeWithDefinition::class, $this->collocation);
        $this->assertInstanceOf(NodeWithTranslations::class, $this->collocation);
        $this->assertInstanceOf(NodeWithSenses::class, $this->collocation);
    }

    function testNodeName(): void {
        $this->assertEquals('collocation', $this->collocation::NODE_NAME);
    }

    function testVarieties(): void {
        $this->collocation->setVarieties(
            new Varieties(
                new Variety('variety 1'),
                new Variety('variety 2'),
            )
        );

        $this->assertEquals(
            new Varieties(
                new Variety('variety 1'),
                new Variety('variety 2'),
            ),
            $this->collocation->getVarieties()
        );
    }

    function testRegisters(): void {
        $this->collocation->setRegisters(
            new Registers(
                new Register('register 1'),
                new Register('register 2'),
            )
        );

        $this->assertEquals(
            new Registers(
                new Register('register 1'),
                new Register('register 2'),
            ),
            $this->collocation->getRegisters()
        );
    }

    function testDomains(): void {
        $this->collocation->setDomains(
            new Domains(
                new Domain('domain 1'),
                new Domain('domain 2'),
            )
        );

        $this->assertEquals(
            new Domains(
                new Domain('domain 1'),
                new Domain('domain 2'),
            ),
            $this->collocation->getDomains()
        );
    }

    function testHeadwords(): void {
        $this->collocation->setHeadwords(
            new Headwords(
                new Headword('headword 1'),
                new Headword('headword 2'),
            )
        );
        $this->assertEquals(
            new Headwords(
                new Headword('headword 1'),
                new Headword('headword 2'),
            ),
            $this->collocation->getHeadwords()
        );
    }

    function testPronunciations(): void {
        $this->collocation->setPronunciations(
            new Pronunciations(
                new Pronunciation('pronunciation 1'),
                new Pronunciation('pronunciation 2')
            )
        );
        $this->assertEquals(
            new Pronunciations(
                new Pronunciation('pronunciation 1'),
                new Pronunciation('pronunciation 2'),
            ),
            $this->collocation->getPronunciations()
        );
    }

    function testDefinition(): void {
        $this->collocation->setDefinition(new Definition('definition'));
        $this->assertEquals(
            new Definition('definition'),
            $this->collocation->getDefinition(),
        );
    }

    function testTranslations(): void {
        $this->collocation
            ->setTranslations(new Translations(
                new Translation('translation 1'),
                new Translation('translation 2'),
            ));
        $this->assertEquals(
            new Translations(
                new Translation('translation 1'),
                new Translation('translation 2'),
            ),
            $this->collocation->getTranslations()
        );
    }

    function testSenses(): void {
        $this->collocation->setSenses(
            new Senses(
                (new Sense())->setTranslations(new Translations(new Translation('translation 1'))),
                (new Sense())->setTranslations(new Translations(new Translation('translation 2'))),
            )
        );
        $this->assertEquals(
            new Senses(
                (new Sense())->setTranslations(new Translations(new Translation('translation 1'))),
                (new Sense())->setTranslations(new Translations(new Translation('translation 2'))),
            ),
            $this->collocation->getSenses()
        );
    }
}
