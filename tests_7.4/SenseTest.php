<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{
    Antonym,
    Collocation,
    Context,
    Definition,
    Domain,
    Headword,
    Node,
    Register,
    Sense,
    Synonym,
    Translation,
    Variety
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Collocations,
    Domains,
    Headwords,
    Registers,
    Senses,
    Synonyms,
    Translations,
    Varieties
};
use Zalmoksis\Dictionary\Model\Interfaces\{
    NodeWithAntonyms,
    NodeWithCollocations,
    NodeWithContext,
    NodeWithDefinition,
    NodeWithSenses,
    NodeWithSynonyms,
    NodeWithTranslations,
};

class SenseTest extends TestCase {
    protected Sense $sense;

    function setUp(): void {
        $this->sense = new Sense();
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->sense);
    }

    function testIfImplementsNodePartials(): void {
        $this->assertInstanceOf(NodeWithContext::class, $this->sense);
        $this->assertInstanceOf(NodeWithDefinition::class, $this->sense);
        $this->assertInstanceOf(NodeWithTranslations::class, $this->sense);
        $this->assertInstanceOf(NodeWithSynonyms::class, $this->sense);
        $this->assertInstanceOf(NodeWithAntonyms::class, $this->sense);
        $this->assertInstanceOf(NodeWithCollocations::class, $this->sense);
        $this->assertInstanceOf(NodeWithSenses::class, $this->sense);
    }

    function testNodeName(): void {
        $this->assertEquals('sense', $this->sense::NODE_NAME);
    }

    function testVarieties(): void {
        $this->sense->setVarieties(
            new Varieties(
                new Variety('variety 1'),
                new Variety('variety 2'),
            )
        );

        $this->assertEquals(
            new Varieties(
                new Variety('variety 1'),
                new Variety('variety 2'),
            ),
            $this->sense->getVarieties()
        );
    }

    function testRegisters(): void {
        $this->sense->setRegisters(
            new Registers(
                new Register('register 1'),
                new Register('register 2'),
            )
        );

        $this->assertEquals(
            new Registers(
                new Register('register 1'),
                new Register('register 2'),
            ),
            $this->sense->getRegisters()
        );
    }

    function testDomains(): void {
        $this->sense->setDomains(
            new Domains(
                new Domain('domain 1'),
                new Domain('domain 2'),
            )
        );

        $this->assertEquals(
            new Domains(
                new Domain('domain 1'),
                new Domain('domain 2'),
            ),
            $this->sense->getDomains()
        );
    }

    function testContext(): void {
        $this->sense->setContext(new Context('context'));
        $this->assertEquals(
            new Context('context'),
            $this->sense->getContext()
        );
    }

    function testDefinition(): void {
        $this->sense->setDefinition(new Definition('definition'));
        $this->assertEquals(
            new Definition('definition'),
            $this->sense->getDefinition()
        );
    }

    function testTranslations(): void {
        $this->sense
            ->setTranslations(new Translations(
                new Translation('translation 1'),
                new Translation('translation 2'),
            ));
        $this->assertEquals(
            new Translations(
                new Translation('translation 1'),
                new Translation('translation 2'),
            ),
            $this->sense->getTranslations()
        );
    }

    function testSynonyms(): void {
        $this->sense->setSynonyms(new Synonyms(
            new Synonym('synonym 1'),
            new Synonym('synonym 2'),
        ));

        $this->assertEquals(
            new Synonyms(
                new Synonym('synonym 1'),
                new Synonym('synonym 2'),
            ),
            $this->sense->getSynonyms()
        );
    }

    function testAntonyms(): void {
        $this->sense->setAntonyms(new Antonyms(
            new Antonym('antonym 1'),
            new Antonym('antonym 2'),
        ));

        $this->assertEquals(
            new Antonyms(
                new Antonym('antonym 1'),
                new Antonym('antonym 2'),
            ),
            $this->sense->getAntonyms()
        );
    }

    function testCollocations(): void {
        $this->sense
            ->setCollocations(new Collocations(
                (new Collocation())
                    ->setHeadwords(new Headwords(new Headword('headword 1')))
                    ->setTranslations(new Translations(new Translation('translation 1'))),
                (new Collocation())
                    ->setHeadwords(new Headwords(new Headword('headword 2')))
                    ->setTranslations(new Translations(new Translation('translation 2'))),
            ));
        $this->assertEquals(
            new Collocations(
                (new Collocation())
                    ->setHeadwords(new Headwords(new Headword('headword 1')))
                    ->setTranslations(new Translations(new Translation('translation 1'))),
                (new Collocation())
                    ->setHeadwords(new Headwords(new Headword('headword 2')))
                    ->setTranslations(new Translations(new Translation('translation 2'))),
            ),
            $this->sense->getCollocations()
        );
    }

    function testSenses(): void {
        $this->sense->setSenses(
            new Senses(
                (new Sense())->setTranslations(new Translations(new Translation('translation 1'))),
                (new Sense())->setTranslations(new Translations(new Translation('translation 2'))),
            )
        );
        $this->assertEquals(
            new Senses(
                (new Sense())->setTranslations(new Translations(new Translation('translation 1'))),
                (new Sense())->setTranslations(new Translations(new Translation('translation 2'))),
            ),
            $this->sense->getSenses()
        );
    }
}
