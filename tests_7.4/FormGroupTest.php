<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\Collections\FormNodes;
use Zalmoksis\Dictionary\Model\{Form, FormGroup, FormLabel, Node};

class FormGroupTest extends TestCase {
    protected FormGroup $formGroup;

    function setUp(): void {
        $this->formGroup = new FormGroup(new FormLabel('form label 1'));
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->formGroup);
    }

    function testNodeName(): void {
        $this->assertEquals('form group', $this->formGroup::NODE_NAME);
    }

    function testFormNodes(): void {
        $this->formGroup->setFormNodes(
            new FormNodes(
                (new Form(new FormLabel('form label 1.1'))),
                (new Form(new FormLabel('form label 1.2'))),
            )
        );
        $this->assertEquals(
            new FormLabel('form label 1'),
            $this->formGroup->getFormLabel()
        );
        $this->assertEquals(
            new FormNodes(
                (new Form(new FormLabel('form label 1.1'))),
                (new Form(new FormLabel('form label 1.2'))),
            ),
            $this->formGroup->getFormNodes()
        );
    }
}
