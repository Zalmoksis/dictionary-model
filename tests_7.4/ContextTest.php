<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Context, Node, Value};

class ContextTest extends TestCase {
    protected Context $context;

    function setUp(): void {
        $this->context = new Context('some context');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->context);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->context);
    }

    function testNodeName(): void {
        $this->assertEquals('context', $this->context::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('some context', $this->context->getValue());
    }
}
