<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{
    Cognate,
    Gloss,
    Language,
    Lemma,
    Node,
};

class CognateTest extends TestCase {
    protected Cognate $cognate;

    function setUp(): void {
        $this->cognate = new Cognate();
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->cognate);
    }

    function testNodeName(): void {
        $this->assertEquals('cognate', $this->cognate::NODE_NAME);
    }

    function testLanguage(): void {
        $this->cognate->setLanguage(new Language('some language'));
        $this->assertEquals(new Language('some language'), $this->cognate->getLanguage());

        $this->cognate->setLanguage(new Language('another language'));
        $this->assertEquals(new Language('another language'), $this->cognate->getLanguage());
    }

    function testLemma(): void {
        $this->cognate->setLemma(new Lemma('some lemma'));
        $this->assertEquals(new Lemma('some lemma'), $this->cognate->getLemma());

        $this->cognate->setLemma(new Lemma('another lemma'));
        $this->assertEquals(new Lemma('another lemma'), $this->cognate->getLemma());
    }

    function testGloss(): void {
        $this->cognate->setGloss(new Gloss('some gloss'));
        $this->assertEquals(new Gloss('some gloss'), $this->cognate->getGloss());

        $this->cognate->setGloss(new Gloss('another gloss'));
        $this->assertEquals(new Gloss('another gloss'), $this->cognate->getGloss());
    }
}
