<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Node, Reference, Synonym};

class SynonymTest extends TestCase {
    protected Synonym $synonym;

    function setUp(): void {
        $this->synonym = new Synonym('someSynonym');
    }

    function testIfImplementsReference(): void {
        $this->assertInstanceOf(Reference::class, $this->synonym);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->synonym);
    }

    function testNodeName(): void {
        $this->assertEquals('synonym', $this->synonym::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('someSynonym', $this->synonym->getHeadword());
    }
}
