<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Gloss, Node, Value};

class GlossTest extends TestCase {
    protected Gloss $gloss;

    function setUp(): void {
        $this->gloss = new Gloss('some gloss');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->gloss);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->gloss);
    }

    function testNodeName(): void {
        $this->assertEquals('gloss', $this->gloss::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('some gloss', $this->gloss->getValue());
    }
}
