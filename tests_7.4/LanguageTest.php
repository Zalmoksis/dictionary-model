<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Language, Node, Value};

class LanguageTest extends TestCase {
    protected Language $language;

    function setUp(): void {
        $this->language = new Language('some language');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->language);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->language);
    }

    function testNodeName(): void {
        $this->assertEquals('language', $this->language::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('some language', $this->language->getValue());
    }
}
