<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests74;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\Collections\Headwords;
use Zalmoksis\Dictionary\Model\{Form, FormLabel, Headword, Node};

class FormTest extends TestCase {
    protected Form $form;

    function setUp(): void {
        $this->form = new Form(new FormLabel('form label 1'));
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->form);
    }

    function testNodeName(): void {
        $this->assertEquals('form', $this->form::NODE_NAME);
    }

    function testHeadwords(): void {
        $this->form
            ->setHeadwords(new Headwords(
                new Headword('headword 1'),
                new Headword('headword 2')
            ));
        $this->assertEquals(
            new FormLabel('form label 1'),
            $this->form->getFormLabel()
        );
        $this->assertEquals(
            new Headwords(
                new Headword('headword 1'),
                new Headword('headword 2')
            ),
            $this->form->getHeadwords()
        );
    }
}
