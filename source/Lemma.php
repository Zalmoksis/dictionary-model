<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Lemma extends Value {
    public const NODE_NAME = 'lemma';

    private bool $hypothetical = false;

    function setHypothetical(bool $hypothetical = true): self {
        $this->hypothetical = $hypothetical;

        return $this;
    }

    function isHypothetical(): bool {
        return $this->hypothetical;
    }
}
