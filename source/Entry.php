<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

use Zalmoksis\Dictionary\Model\Interfaces\{
    NodeWithAntonyms,
    NodeWithCategories,
    NodeWithCognates,
    NodeWithCollocations,
    NodeWithDefinition,
    NodeWithDerivatives,
    NodeWithDomains,
    NodeWithEtymons,
    NodeWithFormNodes,
    NodeWithHeadwords,
    NodeWithHomographIndex,
    NodeWithPronunciations,
    NodeWithRegisters,
    NodeWithSenses,
    NodeWithSoundChanges,
    NodeWithSynonyms,
    NodeWithTranslations,
    NodeWithVarieties
};
use Zalmoksis\Dictionary\Model\Traits\{
    HasAntonyms,
    HasCategories,
    HasCognates,
    HasCollocations,
    HasDefinition,
    HasDerivatives,
    HasDomains,
    HasEtymons,
    HasFormNodes,
    HasHeadwords,
    HasHomographIndex,
    HasPronunciations,
    HasRegisters,
    HasSenses,
    HasSoundChanges,
    HasSynonyms,
    HasTranslations,
    HasVarieties,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Cognates,
    Collocations,
    Derivatives,
    Domains,
    Etymons,
    FormNodes,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    SoundChanges,
    Synonyms,
    Translations,
    Varieties,
};

class Entry extends Node implements
    NodeWithHeadwords,
    NodeWithHomographIndex,
    NodeWithPronunciations,
    NodeWithCategories,
    NodeWithFormNodes,
    NodeWithVarieties,
    NodeWithRegisters,
    NodeWithDomains,
    NodeWithDefinition,
    NodeWithTranslations,
    NodeWithSynonyms,
    NodeWithAntonyms,
    NodeWithCollocations,
    NodeWithSenses,
    NodeWithDerivatives,
    NodeWithEtymons,
    NodeWithSoundChanges,
    NodeWithCognates
{
    public const NODE_NAME = 'entry';

    use HasHeadwords;
    use HasHomographIndex;
    use HasPronunciations;
    use HasCategories;
    use HasFormNodes;
    use HasVarieties;
    use HasRegisters;
    use HasDomains;
    use HasDefinition;
    use HasTranslations;
    use HasSynonyms;
    use HasAntonyms;
    use HasCollocations;
    use HasSenses;
    use HasDerivatives;
    use HasEtymons;
    use HasSoundChanges;
    use HasCognates;

    function __construct(
        ?Headwords $headwords = null,
        ?HomographIndex $homographIndex = null,
        ?Pronunciations $pronunciations = null,
        ?Categories $categories = null,
        ?FormNodes $formNodes = null,
        ?Varieties $varieties = null,
        ?Registers $registers = null,
        ?Domains $domains = null,
        ?Definition $definition = null,
        ?Translations $translations = null,
        ?Synonyms $synonyms = null,
        ?Antonyms $antonyms = null,
        ?Collocations $collocations = null,
        ?Senses $senses = null,
        ?Derivatives $derivatives = null,
        ?Etymons $etymons = null,
        ?SoundChanges $soundChanges = null,
        ?Cognates $cognates = null
    ) {
        $this->headwords = $headwords;
        $this->homographIndex = $homographIndex;
        $this->pronunciations = $pronunciations;
        $this->categories = $categories;
        $this->formNodes = $formNodes;
        $this->varieties = $varieties;
        $this->registers = $registers;
        $this->domains = $domains;
        $this->definition = $definition;
        $this->translations = $translations;
        $this->synonyms = $synonyms;
        $this->antonyms = $antonyms;
        $this->collocations = $collocations;
        $this->senses = $senses;
        $this->derivatives = $derivatives;
        $this->etymons = $etymons;
        $this->soundChanges = $soundChanges;
        $this->cognates = $cognates;
    }
}
