<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class SoundChange extends Value {
    public const NODE_NAME = 'sound change';
}
