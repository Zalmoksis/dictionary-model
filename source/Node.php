<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

abstract class Node {
    public const NODE_NAME = 'node';
}
