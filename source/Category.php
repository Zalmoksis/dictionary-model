<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Category extends Value {
    public const NODE_NAME = 'category';
}
