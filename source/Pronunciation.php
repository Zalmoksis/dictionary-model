<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Pronunciation extends Value {
    public const NODE_NAME = 'pronunciation';
}
