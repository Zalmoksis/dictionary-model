<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Varieties;

interface NodeWithVarieties {
    /** @deprecated */
    function setVarieties(Varieties $varieties): self;
    function getVarieties(): ?Varieties;
}
