<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Domains;

interface NodeWithDomains {
    /** @deprecated */
    function setDomains(Domains $domains): self;
    function getDomains(): ?Domains;
}
