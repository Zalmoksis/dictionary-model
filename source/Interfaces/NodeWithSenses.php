<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Senses;

interface NodeWithSenses {
    /** @deprecated */
    function setSenses(Senses $senses): self;
    function getSenses(): ?Senses;
}
