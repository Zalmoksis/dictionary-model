<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\SoundChanges;

interface NodeWithSoundChanges {
    /** @deprecated */
    function setSoundChanges(SoundChanges $soundChanges): self;
    function getSoundChanges(): ?SoundChanges;
}
