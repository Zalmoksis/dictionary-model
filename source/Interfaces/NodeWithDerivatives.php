<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Derivatives;

interface NodeWithDerivatives {
    /** @deprecated */
    function setDerivatives(Derivatives $derivatives): self;
    function getDerivatives(): ?Derivatives;
}
