<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Pronunciations;

interface NodeWithPronunciations {
    /** @deprecated */
    function setPronunciations(Pronunciations $pronunciations): self;
    function getPronunciations(): ?Pronunciations;
}
