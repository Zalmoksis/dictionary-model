<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Etymons;

interface NodeWithEtymons {
    /** @deprecated */
    function setEtymons(Etymons $etymons): self;
    function getEtymons(): ?Etymons;
}
