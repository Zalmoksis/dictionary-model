<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\FormNodes;

interface NodeWithFormNodes {
    /** @deprecated */
    function setFormNodes(FormNodes $formNodes): self;
    function getFormNodes(): ?FormNodes;
}
