<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Categories;

interface NodeWithCategories {
    /** @deprecated */
    function setCategories(Categories $categories): self;
    function getCategories(): ?Categories;
}
