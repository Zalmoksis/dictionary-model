<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Definition;

interface NodeWithDefinition {
    /** @deprecated */
    function setDefinition(Definition $definition): self;
    function getDefinition(): ?Definition;
}
