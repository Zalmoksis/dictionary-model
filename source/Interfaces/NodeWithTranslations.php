<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Translations;

interface NodeWithTranslations {
    /** @deprecated */
    function setTranslations(Translations $translations): self;
    function getTranslations(): ?Translations;
}
