<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\HomographIndex;

interface NodeWithHomographIndex {
    /** @deprecated */
    function setHomographIndex(HomographIndex $homographIndex): self;
    function getHomographIndex(): ?HomographIndex;
}
