<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Context;

interface NodeWithContext {
    /** @deprecated */
    function setContext(Context $context): self;
    function getContext(): ?Context;
}
