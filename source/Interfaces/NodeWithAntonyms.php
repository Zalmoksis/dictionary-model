<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Antonyms;

interface NodeWithAntonyms {
    /** @deprecated */
    function setAntonyms(Antonyms $antonyms): self;
    function getAntonyms(): ?Antonyms;
}
