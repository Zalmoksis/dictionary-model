<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Headwords;

interface NodeWithHeadwords {
    /** @deprecated */
    function setHeadwords(Headwords $headwords): self;
    function getHeadwords(): ?Headwords;
}
