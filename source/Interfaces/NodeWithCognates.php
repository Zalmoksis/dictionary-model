<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Cognates;

interface NodeWithCognates {
    /** @deprecated */
    function setCognates(Cognates $cognates): self;
    function getCognates(): ?Cognates;
}
