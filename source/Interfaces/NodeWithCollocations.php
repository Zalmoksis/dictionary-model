<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Collocations;

interface NodeWithCollocations {
    /** @deprecated */
    function setCollocations(Collocations $collocations): self;
    function getCollocations(): ?Collocations;
}
