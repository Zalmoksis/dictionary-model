<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Registers;

interface NodeWithRegisters {
    /** @deprecated */
    function setRegisters(Registers $registers): self;
    function getRegisters(): ?Registers;
}
