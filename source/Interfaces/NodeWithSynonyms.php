<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Interfaces;

use Zalmoksis\Dictionary\Model\Collections\Synonyms;

interface NodeWithSynonyms {
    /** @deprecated */
    function setSynonyms(Synonyms $synonyms): self;
    function getSynonyms(): ?Synonyms;
}
