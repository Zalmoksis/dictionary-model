<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Headword extends Value {
    public const NODE_NAME = 'headword';
}
