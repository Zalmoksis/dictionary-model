<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\FormLabel;

trait HasFormLabel {
    protected ?FormLabel $formLabel = null;

    /** @deprecated */
    function setFormLabel(FormLabel $formLabel): self {
        $this->formLabel = $formLabel;

        return $this;
    }

    function getFormLabel(): ?FormLabel {
        return $this->formLabel;
    }
}
