<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Registers;

trait HasRegisters {
    private ?Registers $registers = null;

    /** @deprecated */
    function setRegisters(Registers $registers): self {
        $this->registers = $registers;

        return $this;
    }

    function getRegisters(): ?Registers {
        return $this->registers;
    }
}
