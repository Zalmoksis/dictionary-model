<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Senses;

trait HasSenses {
    protected ?Senses $senses = null;

    /** @deprecated */
    function setSenses(Senses $senses): self {
        $this->senses = $senses;

        return $this;
    }

    function getSenses(): ?Senses {
        return $this->senses;
    }
}
