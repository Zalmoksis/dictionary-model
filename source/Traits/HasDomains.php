<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Domains;

trait HasDomains {
    private ?Domains $domains = null;

    /** @deprecated */
    function setDomains(Domains $domains): self {
        $this->domains = $domains;

        return $this;
    }

    function getDomains(): ?Domains {
        return $this->domains;
    }
}
