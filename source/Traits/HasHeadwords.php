<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Headwords;

trait HasHeadwords {
    protected ?Headwords $headwords = null;

    /** @deprecated */
    function setHeadwords(Headwords $headwords): self {
        $this->headwords = $headwords;

        return $this;
    }

    function getHeadwords(): ?Headwords {
        return $this->headwords;
    }
}
