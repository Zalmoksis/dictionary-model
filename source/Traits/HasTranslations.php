<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Translations;

trait HasTranslations {
    protected ?Translations $translations = null;

    /** @deprecated */
    function setTranslations(Translations $translations): self {
        $this->translations = $translations;

        return $this;
    }

    function getTranslations(): ?Translations {
        return $this->translations;
    }
}
