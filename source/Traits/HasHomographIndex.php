<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\HomographIndex;

trait HasHomographIndex {
    protected ?HomographIndex $homographIndex = null;

    /** @deprecated */
    function setHomographIndex(HomographIndex $homographIndex): self {
        $this->homographIndex = $homographIndex;

        return $this;
    }

    function getHomographIndex(): ?HomographIndex {
        return $this->homographIndex;
    }
}
