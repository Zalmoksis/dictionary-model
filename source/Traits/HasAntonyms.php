<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Antonyms;

trait HasAntonyms {
    private ?Antonyms $antonyms = null;

    /** @deprecated */
    function setAntonyms(Antonyms $antonyms): self {
        $this->antonyms = $antonyms;

        return $this;
    }

    function getAntonyms(): ?Antonyms {
        return $this->antonyms;
    }
}
