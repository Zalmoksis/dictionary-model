<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Synonyms;

trait HasSynonyms {
    private ?Synonyms $synonyms = null;

    /** @deprecated */
    function setSynonyms(Synonyms $synonyms): self {
        $this->synonyms = $synonyms;

        return $this;
    }

    function getSynonyms(): ?Synonyms {
        return $this->synonyms;
    }
}
