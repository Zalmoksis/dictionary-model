<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Etymons;

trait HasEtymons {
    protected ?Etymons $etymons = null;

    /** @deprecated */
    function setEtymons(Etymons $etymons): self {
        $this->etymons = $etymons;

        return $this;
    }

    function getEtymons(): ?Etymons {
        return $this->etymons;
    }
}
