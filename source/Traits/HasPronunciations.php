<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Pronunciations;

trait HasPronunciations {
    protected ?Pronunciations $pronunciations = null;

    /** @deprecated */
    function setPronunciations(Pronunciations $pronunciations): self {
        $this->pronunciations = $pronunciations;

        return $this;
    }

    function getPronunciations(): ?Pronunciations {
        return $this->pronunciations;
    }
}
