<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Categories;

trait HasCategories {
    protected ?Categories $categories = null;

    /** @deprecated */
    function setCategories(Categories $categories): self {
        $this->categories = $categories;

        return $this;
    }

    function getCategories(): ?Categories {
        return $this->categories;
    }
}
