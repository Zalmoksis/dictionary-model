<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Cognates;

trait HasCognates {
    protected ?Cognates $cognates = null;

    /** @deprecated */
    function setCognates(Cognates $cognates): self {
        $this->cognates = $cognates;

        return $this;
    }

    function getCognates(): ?Cognates {
        return $this->cognates;
    }
}
