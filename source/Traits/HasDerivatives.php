<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Derivatives;

trait HasDerivatives {
    private ?Derivatives $derivatives = null;

    /** @deprecated */
    function setDerivatives(Derivatives $derivatives): self {
        $this->derivatives = $derivatives;

        return $this;
    }

    function getDerivatives(): ?Derivatives {
        return $this->derivatives;
    }
}
