<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Language;

trait HasLanguage {
    protected ?Language $language = null;

    /** @deprecated */
    function setLanguage(Language $language): self {
        $this->language = $language;

        return $this;
    }

    function getLanguage(): ?Language {
        return $this->language;
    }
}
