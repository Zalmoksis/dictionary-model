<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Varieties;

trait HasVarieties {
    private ?Varieties $varieties = null;

    /** @deprecated */
    function setVarieties(Varieties $varieties): self {
        $this->varieties = $varieties;

        return $this;
    }

    function getVarieties(): ?Varieties {
        return $this->varieties;
    }
}
