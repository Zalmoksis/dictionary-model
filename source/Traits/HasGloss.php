<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Gloss;

trait HasGloss {
    protected ?Gloss $gloss = null;

    /** @deprecated */
    function setGloss(Gloss $gloss): self {
        $this->gloss = $gloss;

        return $this;
    }

    function getGloss(): ?Gloss {
        return $this->gloss;
    }
}
