<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\FormNodes;

trait HasFormNodes {
    protected ?FormNodes $formNodes = null;

    /** @deprecated */
    function setFormNodes(FormNodes $formNodes): self {
        $this->formNodes = $formNodes;

        return $this;
    }

    function getFormNodes(): ?FormNodes {
        return $this->formNodes;
    }
}
