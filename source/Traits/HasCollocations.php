<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\Collocations;

trait HasCollocations {
    protected ?Collocations $collocations = null;

    /** @deprecated */
    function setCollocations(Collocations $collocations): self {
        $this->collocations = $collocations;

        return $this;
    }

    function getCollocations(): ?Collocations {
        return $this->collocations;
    }
}
