<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Definition;

trait HasDefinition {
    protected ?Definition $definition = null;

    /** @deprecated */
    function setDefinition(Definition $definition): self {
        $this->definition = $definition;

        return $this;
    }

    function getDefinition(): ?Definition {
        return $this->definition;
    }
}
