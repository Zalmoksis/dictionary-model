<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Lemma;

trait HasLemma {
    protected ?Lemma $lemma = null;

    /** @deprecated */
    function setLemma(Lemma $lemma): self {
        $this->lemma = $lemma;

        return $this;
    }

    function getLemma(): ?Lemma {
        return $this->lemma;
    }
}
