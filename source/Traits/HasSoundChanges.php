<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Collections\SoundChanges;

trait HasSoundChanges {
    private ?SoundChanges $soundChanges = null;

    /** @deprecated */
    function setSoundChanges(SoundChanges $soundChanges): self {
        $this->soundChanges = $soundChanges;

        return $this;
    }

    function getSoundChanges(): ?SoundChanges {
        return $this->soundChanges;
    }
}
