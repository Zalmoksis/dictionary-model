<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Traits;

use Zalmoksis\Dictionary\Model\Context;

trait HasContext {
    protected ?Context $context = null;

    /** @deprecated */
    function setContext(Context $context): self {
        $this->context = $context;

        return $this;
    }

    function getContext(): ?Context {
        return $this->context;
    }
}
