<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

use Zalmoksis\Dictionary\Model\Traits\{HasGloss, HasLanguage, HasLemma};

class Etymon extends Node {
    public const NODE_NAME = 'etymon';

    use HasLanguage;
    use HasLemma;
    use HasGloss;

    function __construct(
        ?Language $language = null,
        ?Lemma $lemma = null,
        ?Gloss $gloss = null
    ) {
        $this->language = $language;
        $this->lemma = $lemma;
        $this->gloss = $gloss;
    }
}
