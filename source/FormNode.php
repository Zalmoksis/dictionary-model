<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

use Zalmoksis\Dictionary\Model\Traits\HasFormLabel;

abstract class FormNode extends Node {
    public const NODE_NAME = 'form node';

    use HasFormLabel;

    function __construct(FormLabel $formLabel = null) {
        $this->formLabel = $formLabel;
    }
}
