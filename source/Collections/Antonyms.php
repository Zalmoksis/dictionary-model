<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Antonym;

class Antonyms extends Nodes {
    public const NODE_COLLECTION_NAME = 'antonyms';

    function __construct(Antonym ...$antonyms) {
        $this->elements = $antonyms;
    }
}
