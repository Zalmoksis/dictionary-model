<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Translation;

class Translations extends Nodes {
    public const NODE_COLLECTION_NAME = 'translations';

    function __construct(Translation ...$translations) {
        $this->elements = $translations;
    }
}
