<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\DataStructures\Collection;

class Nodes extends Collection {
    public const NODE_COLLECTION_NAME = 'nodes';
}
