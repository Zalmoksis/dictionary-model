<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Domain;

class Domains extends Nodes {
    public const NODE_COLLECTION_NAME = 'domains';

    function __construct(Domain ...$domains) {
        $this->elements = $domains;
    }
}
