<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Entry;

class Entries extends Nodes {
    public const NODE_COLLECTION_NAME = 'entries';

    function __construct(Entry ...$entries) {
        $this->elements = $entries;
    }
}
