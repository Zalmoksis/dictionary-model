<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Synonym;

class Synonyms extends Nodes {
    public const NODE_COLLECTION_NAME = 'synonyms';

    function __construct(Synonym ...$synonyms) {
        $this->elements = $synonyms;
    }
}
