<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Category;

class Categories extends Nodes {
    public const NODE_COLLECTION_NAME = 'categories';

    function __construct(Category ...$categories) {
        $this->elements = $categories;
    }
}
