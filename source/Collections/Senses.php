<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Sense;

class Senses extends Nodes {
    public const NODE_COLLECTION_NAME = 'senses';

    function __construct(Sense ...$senses) {
        $this->elements = $senses;
    }
}
