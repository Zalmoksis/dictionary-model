<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Headword;

class Headwords extends Nodes {
    public const NODE_COLLECTION_NAME = 'headwords';

    function __construct(Headword ...$headwords) {
        $this->elements = $headwords;
    }

    function getFirst(): ?Headword {
        return $this->elements[0] ?? null;
    }
}
