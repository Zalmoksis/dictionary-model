<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Pronunciation;

class Pronunciations extends Nodes {
    public const NODE_COLLECTION_NAME = 'pronunciations';

    function __construct(Pronunciation ...$pronunciations) {
        $this->elements = $pronunciations;
    }
}
