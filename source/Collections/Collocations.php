<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Collocation;

class Collocations extends Nodes {
    public const NODE_COLLECTION_NAME = 'collocations';

    function __construct(Collocation ...$collocations) {
        $this->elements = $collocations;
    }
}
