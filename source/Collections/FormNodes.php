<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\FormNode;

class FormNodes extends Nodes {
    public const NODE_COLLECTION_NAME = 'form nodes';

    function __construct(FormNode ...$formNodes) {
        $this->elements = $formNodes;
    }
}
