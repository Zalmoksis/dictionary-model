<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Etymon;

class Etymons extends Nodes {
    public const NODE_COLLECTION_NAME = 'etymons';

    function __construct(Etymon ...$etymons) {
        $this->elements = $etymons;
    }
}
