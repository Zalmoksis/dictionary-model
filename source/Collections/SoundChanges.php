<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\SoundChange;

class SoundChanges extends Nodes {
    public const NODE_COLLECTION_NAME = 'sound changes';

    function __construct(SoundChange ...$headwords) {
        $this->elements = $headwords;
    }
}
