<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Variety;

class Varieties extends Nodes {
    public const NODE_COLLECTION_NAME = 'varieties';

    function __construct(Variety ...$varieties) {
        $this->elements = $varieties;
    }
}
