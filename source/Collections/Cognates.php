<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Cognate;

class Cognates extends Nodes {
    public const NODE_COLLECTION_NAME = 'cognates';

    function __construct(Cognate ...$cognates) {
        $this->elements = $cognates;
    }
}
