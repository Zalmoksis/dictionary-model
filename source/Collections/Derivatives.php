<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Derivative;

class Derivatives extends Nodes {
    public const NODE_COLLECTION_NAME = 'derivatives';

    function __construct(Derivative ...$derivatives) {
        $this->elements = $derivatives;
    }
}
