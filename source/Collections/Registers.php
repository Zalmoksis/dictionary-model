<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Collections;

use Zalmoksis\Dictionary\Model\Register;

class Registers extends Nodes {
    public const NODE_COLLECTION_NAME = 'registers';

    function __construct(Register ...$registers) {
        $this->elements = $registers;
    }
}
