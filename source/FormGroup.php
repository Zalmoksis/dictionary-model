<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

use Zalmoksis\Dictionary\Model\{Collections\FormNodes, Traits\HasFormNodes};

class FormGroup extends FormNode {
    public const NODE_NAME = 'form group';

    use HasFormNodes;

    function __construct(
        FormLabel $formLabel = null,
        FormNodes $formNodes = null
    ) {
        parent::__construct($formLabel);
        $this->formNodes = $formNodes;
    }
}
