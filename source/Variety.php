<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Variety extends Value {
    public const NODE_NAME = 'variety';
}
