<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Definition extends Value {
    public const NODE_NAME = 'definition';
}
