<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

abstract class Value extends Node {
    public const NODE_NAME = 'value';

    protected string $value;

    function __construct(string $value) {
        $this->value = $value;
    }

    function getValue(): string {
        return $this->value;
    }
}
