<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Derivative extends Reference {
    public const NODE_NAME = 'derivative';
}
