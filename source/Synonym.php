<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Synonym extends Reference {
    public const NODE_NAME = 'synonym';
}
