<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class HomographIndex extends Node {
    public const NODE_NAME = 'homograph_index';

    private int $value;

    function __construct(int $value) {
        $this->value = $value;
    }

    public function getValue(): int {
        return $this->value;
    }
}
