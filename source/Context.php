<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Context extends Value {
    public const NODE_NAME = 'context';
}
