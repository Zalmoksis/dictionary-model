<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Language extends Value {
    public const NODE_NAME = 'language';
}
