<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Domain extends Value {
    public const NODE_NAME = 'domain';
}
