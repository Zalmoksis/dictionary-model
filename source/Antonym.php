<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Antonym extends Reference {
    public const NODE_NAME = 'antonym';
}
