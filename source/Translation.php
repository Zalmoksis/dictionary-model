<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Translation extends Value {
    public const NODE_NAME = 'translation';
}
