<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

use Zalmoksis\Dictionary\Model\Interfaces\{
    NodeWithAntonyms,
    NodeWithCollocations,
    NodeWithContext,
    NodeWithDefinition,
    NodeWithDomains,
    NodeWithRegisters,
    NodeWithSenses,
    NodeWithSynonyms,
    NodeWithTranslations,
    NodeWithVarieties,
};
use Zalmoksis\Dictionary\Model\Traits\{
    HasAntonyms,
    HasCollocations,
    HasContext,
    HasDefinition,
    HasDomains,
    HasRegisters,
    HasSenses,
    HasSynonyms,
    HasTranslations,
    HasVarieties,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Collocations,
    Domains,
    Registers,
    Senses,
    Synonyms,
    Translations,
    Varieties,
};

class Sense extends Node implements
    NodeWithVarieties,
    NodeWithRegisters,
    NodeWithDomains,
    NodeWithContext,
    NodeWithDefinition,
    NodeWithTranslations,
    NodeWithSynonyms,
    NodeWithAntonyms,
    NodeWithCollocations,
    NodeWithSenses
{
    public const NODE_NAME = 'sense';

    use HasVarieties;
    use HasRegisters;
    use HasDomains;
    use HasContext;
    use HasDefinition;
    use HasTranslations;
    use HasSynonyms;
    use HasAntonyms;
    use HasCollocations;
    use HasSenses;

    function __construct(
        ?Varieties $varieties = null,
        ?Registers $registers = null,
        ?Domains $domains = null,
        ?Definition $definition = null,
        ?Context $context = null,
        ?Translations $translations = null,
        ?Synonyms $synonyms = null,
        ?Antonyms $antonyms = null,
        ?Collocations $collocations = null,
        ?Senses $senses = null
    ) {
        $this->varieties = $varieties;
        $this->registers = $registers;
        $this->domains = $domains;
        $this->context = $context;
        $this->definition = $definition;
        $this->translations = $translations;
        $this->synonyms = $synonyms;
        $this->antonyms = $antonyms;
        $this->collocations = $collocations;
        $this->senses = $senses;
    }
}
