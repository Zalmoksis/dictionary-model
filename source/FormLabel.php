<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class FormLabel extends Value {
    public const NODE_NAME = 'form label';
}
