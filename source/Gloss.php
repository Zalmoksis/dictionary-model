<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Gloss extends Value {
    public const NODE_NAME = 'gloss';
}
