<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

class Register extends Value {
    public const NODE_NAME = 'register';
}
