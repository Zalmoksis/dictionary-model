<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

use Zalmoksis\Dictionary\Model\Interfaces\{
    NodeWithDefinition,
    NodeWithDomains,
    NodeWithHeadwords,
    NodeWithPronunciations,
    NodeWithRegisters,
    NodeWithSenses,
    NodeWithTranslations,
    NodeWithVarieties,
};
use Zalmoksis\Dictionary\Model\Traits\{
    HasDefinition,
    HasDomains,
    HasHeadwords,
    HasPronunciations,
    HasRegisters,
    HasSenses,
    HasTranslations,
    HasVarieties,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Domains,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    Translations,
    Varieties,
};

class Collocation extends Node implements
    NodeWithHeadwords,
    NodeWithPronunciations,
    NodeWithVarieties,
    NodeWithRegisters,
    NodeWithDomains,
    NodeWithDefinition,
    NodeWithTranslations,
    NodeWithSenses
{
    public const NODE_NAME = 'collocation';

    use HasHeadwords;
    use HasPronunciations;
    use HasVarieties;
    use HasRegisters;
    use HasDomains;
    use HasDefinition;
    use HasTranslations;
    use HasSenses;

    function __construct(
        ?Headwords $headwords = null,
        ?Pronunciations $pronunciations = null,
        ?Varieties $varieties = null,
        ?Registers $registers = null,
        ?Domains $domains = null,
        ?Definition $definition = null,
        ?Translations $translations = null,
        ?Senses $senses = null
    ) {
        $this->headwords = $headwords;
        $this->pronunciations = $pronunciations;
        $this->varieties = $varieties;
        $this->registers = $registers;
        $this->domains = $domains;
        $this->definition = $definition;
        $this->translations = $translations;
        $this->senses = $senses;
    }
}
