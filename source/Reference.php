<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

abstract class Reference extends Node {
    private string $headword;

    public const NODE_NAME = 'reference';

    function __construct(string $headword) {
        $this->headword = $headword;
    }

    function getHeadword(): string {
        return $this->headword;
    }
}
