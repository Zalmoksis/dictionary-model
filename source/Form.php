<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model;

use Zalmoksis\Dictionary\Model\{Collections\Headwords, Traits\HasHeadwords};

class Form extends FormNode {
    public const NODE_NAME = 'form';

    use HasHeadwords;

    function __construct(
        FormLabel $formLabel = null,
        Headwords $headwords = null
    ) {
        parent::__construct($formLabel);
        $this->headwords = $headwords;
    }
}
