<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Node, Translation, Value};

class TranslationTest extends TestCase {
    protected Translation $translation;

    function setUp(): void {
        $this->translation = new Translation('someTranslation');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->translation);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->translation);
    }

    function testNodeName(): void {
        $this->assertEquals('translation', $this->translation::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('someTranslation', $this->translation->getValue());
    }
}
