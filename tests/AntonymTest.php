<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Antonym, Node, Reference};

class AntonymTest extends TestCase {
    protected Antonym $antonym;

    function setUp(): void {
        $this->antonym = new Antonym('someAntonym');
    }

    function testIfImplementsReference(): void {
        $this->assertInstanceOf(Reference::class, $this->antonym);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->antonym);
    }

    function testNodeName(): void {
        $this->assertEquals('antonym', $this->antonym::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('someAntonym', $this->antonym->getHeadword());
    }
}
