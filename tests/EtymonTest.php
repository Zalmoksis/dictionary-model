<?php

/** @noinspection PhpLanguageLevelInspection */

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Etymon, Gloss, Language, Lemma, Node};

class EtymonTest extends TestCase {

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, new Etymon());
    }

    function testNodeName(): void {
        $this->assertEquals('etymon', Etymon::NODE_NAME);
    }

    function testLanguage(): void {
        $this->assertNull((new Etymon())->getLanguage());

        $etymon = new Etymon(language: new Language('some language'));

        $this->assertEquals(new Language('some language'), $etymon->getLanguage());
    }

    function testLemma(): void {
        $this->assertNull((new Etymon())->getLemma());

        $etymon = new Etymon(lemma: new Lemma('some lemma'));
        $this->assertEquals(new Lemma('some lemma'), $etymon->getLemma());
    }

    function testGloss(): void {
        $this->assertNull((new Etymon())->getGloss());

        $etymon = new Etymon(gloss: new Gloss('some gloss'));
        $this->assertEquals(new Gloss('some gloss'), $etymon->getGloss());
    }
}
