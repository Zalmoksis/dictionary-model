<?php

/** @noinspection PhpLanguageLevelInspection */

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{
    Antonym,
    Collocation,
    Context,
    Definition,
    Domain,
    Headword,
    Node,
    Register,
    Sense,
    Synonym,
    Translation,
    Variety
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Collocations,
    Domains,
    Headwords,
    Registers,
    Senses,
    Synonyms,
    Translations,
    Varieties
};
use Zalmoksis\Dictionary\Model\Interfaces\{
    NodeWithAntonyms,
    NodeWithCollocations,
    NodeWithContext,
    NodeWithDefinition,
    NodeWithDomains,
    NodeWithRegisters,
    NodeWithSenses,
    NodeWithSynonyms,
    NodeWithTranslations,
    NodeWithVarieties,
};

class SenseTest extends TestCase {

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, new Sense());
    }

    function testIfImplementsNodePartials(): void {
        $sense = new Sense();
        $this->assertInstanceOf(NodeWithVarieties::class, $sense);
        $this->assertInstanceOf(NodeWithRegisters::class, $sense);
        $this->assertInstanceOf(NodeWithDomains::class, $sense);
        $this->assertInstanceOf(NodeWithContext::class, $sense);
        $this->assertInstanceOf(NodeWithDefinition::class, $sense);
        $this->assertInstanceOf(NodeWithTranslations::class, $sense);
        $this->assertInstanceOf(NodeWithSynonyms::class, $sense);
        $this->assertInstanceOf(NodeWithAntonyms::class, $sense);
        $this->assertInstanceOf(NodeWithCollocations::class, $sense);
        $this->assertInstanceOf(NodeWithSenses::class, $sense);
    }

    function testNodeName(): void {
        $this->assertEquals('sense', Sense::NODE_NAME);
    }

    function testVarieties(): void {
        $this->assertNull((new Sense())->getVarieties());

        $sense = new Sense(
            varieties: new Varieties(
                new Variety('variety 1'),
                new Variety('variety 2'),
            )
        );

        $this->assertEquals(
            new Varieties(
                new Variety('variety 1'),
                new Variety('variety 2'),
            ),
            $sense->getVarieties()
        );
    }

    function testRegisters(): void {
        $this->assertNull((new Sense())->getRegisters());

        $sense = new Sense(
            registers: new Registers(
                new Register('register 1'),
                new Register('register 2'),
            )
        );

        $this->assertEquals(
            new Registers(
                new Register('register 1'),
                new Register('register 2'),
            ),
            $sense->getRegisters()
        );
    }

    function testDomains(): void {
        $this->assertNull((new Sense())->getDomains());

        $sense = new Sense(
            domains: new Domains(
                new Domain('domain 1'),
                new Domain('domain 2'),
            )
        );

        $this->assertEquals(
            new Domains(
                new Domain('domain 1'),
                new Domain('domain 2'),
            ),
            $sense->getDomains()
        );
    }

    function testContext(): void {
        $this->assertNull((new Sense())->getContext());

        $sense = new Sense(context: new Context('context'));

        $this->assertEquals(
            new Context('context'),
            $sense->getContext()
        );
    }

    function testDefinition(): void {
        $this->assertNull((new Sense())->getDefinition());

        $sense = new Sense(definition: new Definition('definition'));

        $this->assertEquals(
            new Definition('definition'),
            $sense->getDefinition()
        );
    }

    function testTranslations(): void {
        $this->assertNull((new Sense())->getTranslations());

        $sense = new Sense(
            translations: new Translations(
                new Translation('translation 1'),
                new Translation('translation 2'),
            ));

        $this->assertEquals(
            new Translations(
                new Translation('translation 1'),
                new Translation('translation 2'),
            ),
            $sense->getTranslations()
        );
    }

    function testSynonyms(): void {
        $this->assertNull((new Sense())->getSynonyms());

        $sense = new Sense(
            synonyms: new Synonyms(
                new Synonym('synonym 1'),
                new Synonym('synonym 2'),
            )
        );

        $this->assertEquals(
            new Synonyms(
                new Synonym('synonym 1'),
                new Synonym('synonym 2'),
            ),
            $sense->getSynonyms()
        );
    }

    function testAntonyms(): void {
        $this->assertNull((new Sense())->getAntonyms());

        $sense = new Sense(
            antonyms: new Antonyms(
                new Antonym('antonym 1'),
                new Antonym('antonym 2'),
        ));

        $this->assertEquals(
            new Antonyms(
                new Antonym('antonym 1'),
                new Antonym('antonym 2'),
            ),
            $sense->getAntonyms()
        );
    }

    function testCollocations(): void {
        $this->assertNull((new Sense())->getCollocations());

        $sense = new Sense(
            collocations: new Collocations(
                new Collocation(
                    headwords: new Headwords(new Headword('headword 1')),
                    translations: new Translations(new Translation('translation 1'))
                ),
                new Collocation(
                    headwords: new Headwords(new Headword('headword 2')),
                    translations: new Translations(new Translation('translation 2'))
                ),
            )
        );

        $this->assertEquals(
            new Collocations(
                new Collocation(
                    headwords: new Headwords(new Headword('headword 1')),
                    translations: new Translations(new Translation('translation 1'))
                ),
                new Collocation(
                    headwords: new Headwords(new Headword('headword 2')),
                    translations: new Translations(new Translation('translation 2'))
                ),
            ),
            $sense->getCollocations()
        );
    }

    function testSenses(): void {
        $this->assertNull((new Sense())->getSenses());

        $sense = new Sense(
            senses: new Senses(
                new Sense(translations: new Translations(new Translation('translation 1'))),
                new Sense(translations: new Translations(new Translation('translation 2'))),
            )
        );

        $this->assertEquals(
            new Senses(
                new Sense(translations: new Translations(new Translation('translation 1'))),
                new Sense(translations: new Translations(new Translation('translation 2'))),
            ),
            $sense->getSenses()
        );
    }
}
