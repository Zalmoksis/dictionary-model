<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Collections\FormNodes, Form, FormGroup};

class FormNodesTest extends TestCase {
    protected FormNodes $formNodes;

    function setUp(): void {
        $this->formNodes = new FormNodes(
            new Form(),
            new FormGroup(),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->formNodes);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->formNodes);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->formNodes);
    }

    function testCollectionName(): void {
        $this->assertEquals('form nodes', $this->formNodes::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->formNodes);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->formNodes as $formNode) {
            $elements[] = $formNode;
        }

        $this->assertEquals([
            new Form(),
            new FormGroup(),
        ], $elements);
    }
}
