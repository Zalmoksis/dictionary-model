<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Antonym, Collections\Antonyms};

class AntonymsTest extends TestCase {
    private Antonyms $antonyms;

    function setUp(): void {
        $this->antonyms = new Antonyms(
            new Antonym('antonym 1'),
            new Antonym('antonym 2'),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->antonyms);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->antonyms);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->antonyms);
    }

    function testCollectionName(): void {
        $this->assertEquals('antonyms', $this->antonyms::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->antonyms);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->antonyms as $antonym) {
            $elements[] = $antonym;
        }

        $this->assertEquals([
            new Antonym('antonym 1'),
            new Antonym('antonym 2'),
        ], $elements);
    }
}
