<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Collections\Registers, Register};

class RegistersTest extends TestCase {
    protected Registers $registers;

    function setUp(): void {
        $this->registers = new Registers(
            new Register('register 1'),
            new Register('register 2'),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->registers);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->registers);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->registers);
    }

    function testCollectionName(): void {
        $this->assertEquals('registers', $this->registers::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->registers);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->registers as $register) {
            $elements[] = $register;
        }

        $this->assertEquals([
            new Register('register 1'),
            new Register('register 2'),
        ], $elements);
    }
}
