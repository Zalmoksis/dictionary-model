<?php

/** @noinspection PhpLanguageLevelInspection */

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Collections\Entries, Collections\Headwords, Entry, Headword};

class EntriesTest extends TestCase {
    protected Entries $entries;

    function setUp(): void {
        $this->entries = new Entries(
            new Entry(headwords: new Headwords(new Headword('entry 1'))),
            new Entry(headwords: new Headwords(new Headword('entry 2'))),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->entries);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->entries);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->entries);
    }

    function testCollectionName(): void {
        $this->assertEquals('entries', $this->entries::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->entries);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->entries as $entry) {
            $elements[] = $entry;
        }

        $this->assertEquals([
            new Entry(headwords: new Headwords(new Headword('entry 1'))),
            new Entry(headwords: new Headwords(new Headword('entry 2'))),
        ], $elements);
    }
}
