<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\Collections\Derivatives;
use Zalmoksis\Dictionary\Model\Derivative;

class DerivativesTest extends TestCase {
    protected Derivatives $derivatives;

    function setUp(): void {
        $this->derivatives = new Derivatives(
            new Derivative('derivative 1'),
            new Derivative('derivative 2'),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->derivatives);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->derivatives);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->derivatives);
    }

    function testCollectionName(): void {
        $this->assertEquals('derivatives', $this->derivatives::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->derivatives);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->derivatives as $derivative) {
            $elements[] = $derivative;
        }

        $this->assertEquals([
            new Derivative('derivative 1'),
            new Derivative('derivative 2'),
        ], $elements);
    }
}
