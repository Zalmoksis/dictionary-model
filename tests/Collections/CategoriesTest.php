<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Category, Collections\Categories};

class CategoriesTest extends TestCase {
    protected Categories $categories;

    function setUp(): void {
        $this->categories = new Categories(
            new Category('category 1'),
            new Category('category 2'),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->categories);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->categories);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->categories);
    }

    function testCollectionName(): void {
        $this->assertEquals('categories', $this->categories::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->categories);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->categories as $category) {
            $elements[] = $category;
        }

        $this->assertEquals([
            new Category('category 1'),
            new Category('category 2'),
        ], $elements);
    }
}
