<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Collections\Synonyms, Synonym};

class SynonymsTest extends TestCase {
    protected Synonyms $synonyms;

    function setUp(): void {
        $this->synonyms = new Synonyms(
            new Synonym('synonym 1'),
            new Synonym('synonym 2'),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->synonyms);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->synonyms);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->synonyms);
    }

    function testCollectionName(): void {
        $this->assertEquals('synonyms', $this->synonyms::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->synonyms);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->synonyms as $synonym) {
            $elements[] = $synonym;
        }

        $this->assertEquals([
            new Synonym('synonym 1'),
            new Synonym('synonym 2'),
        ], $elements);
    }
}
