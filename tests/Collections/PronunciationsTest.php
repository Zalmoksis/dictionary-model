<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Collections\Pronunciations, Pronunciation};

class PronunciationsTest extends TestCase {
    protected Pronunciations $pronunciations;

    function setUp(): void {
        $this->pronunciations = new Pronunciations(
            new Pronunciation('pronunciation 1'),
            new Pronunciation('pronunciation 2'),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->pronunciations);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->pronunciations);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->pronunciations);
    }

    function testCollectionName(): void {
        $this->assertEquals('pronunciations', $this->pronunciations::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->pronunciations);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->pronunciations as $pronunciation) {
            $elements[] = $pronunciation;
        }

        $this->assertEquals([
            new Pronunciation('pronunciation 1'),
            new Pronunciation('pronunciation 2'),
        ], $elements);
    }
}
