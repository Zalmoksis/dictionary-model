<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Collections\Varieties, Variety};

class VarietiesTest extends TestCase {
    protected Varieties $varieties;

    function setUp(): void {
        $this->varieties = new Varieties(
            new Variety('variety 1'),
            new Variety('variety 2'),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->varieties);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->varieties);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->varieties);
    }

    function testCollectionName(): void {
        $this->assertEquals('varieties', $this->varieties::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->varieties);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->varieties as $variety) {
            $elements[] = $variety;
        }

        $this->assertEquals([
            new Variety('variety 1'),
            new Variety('variety 2'),
        ], $elements);
    }
}
