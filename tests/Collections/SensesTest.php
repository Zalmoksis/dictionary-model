<?php

/** @noinspection PhpLanguageLevelInspection */

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests\Collections;

use Countable;
use PHPUnit\Framework\TestCase;
use Traversable;
use Zalmoksis\DataStructures\Collection;
use Zalmoksis\Dictionary\Model\{Collections\Senses, Collections\Translations, Sense, Translation};

class SensesTest extends TestCase {
    protected Senses $senses;

    function setUp(): void {
        $this->senses = new Senses(
            new Sense(translations: new Translations(new Translation('sense 1'))),
            new Sense(translations: new Translations(new Translation('sense 2'))),
        );
    }

    function testIfImplementsTraversable(): void {
        $this->assertInstanceOf(Traversable::class, $this->senses);
    }

    function testIfImplementsCountable(): void {
        $this->assertInstanceOf(Countable::class, $this->senses);
    }

    function testIfImplementsCollection(): void {
        $this->assertInstanceOf(Collection::class, $this->senses);
    }

    function testCollectionName(): void {
        $this->assertEquals('senses', $this->senses::NODE_COLLECTION_NAME);
    }

    function testCounting(): void {
        $this->assertCount(2, $this->senses);
    }

    function testIterating(): void {
        $elements = [];

        foreach ($this->senses as $sense) {
            $elements[] = $sense;
        }

        $this->assertEquals([
            new Sense(translations: new Translations(new Translation('sense 1'))),
            new Sense(translations: new Translations(new Translation('sense 2'))),
        ], $elements);
    }
}
