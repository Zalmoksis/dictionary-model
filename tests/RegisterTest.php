<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Node, Register, Value};

class RegisterTest extends TestCase {
    protected Register $register;

    function setUp(): void {
        $this->register = new Register('some register');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->register);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->register);
    }

    function testNodeName(): void {
        $this->assertEquals('register', $this->register::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('some register', $this->register->getValue());
    }
}
