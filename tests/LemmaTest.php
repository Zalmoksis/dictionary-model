<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Lemma, Node, Value};

class LemmaTest extends TestCase {
    protected Lemma $lemma;

    function setUp(): void {
        $this->lemma = new Lemma('some lemma');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->lemma);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->lemma);
    }

    function testNodeName(): void {
        $this->assertEquals('lemma', $this->lemma::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('some lemma', $this->lemma->getValue());
    }

    function testIfNotHypotheticalByDefault(): void {
        $this->assertFalse($this->lemma->isHypothetical());
    }

    function testHypotheticalIfSetTrue(): void {
        $this->assertTrue($this->lemma->setHypothetical(true)->isHypothetical());
    }

    function testHypotheticalIfSetFalse(): void {
        $this->assertFalse($this->lemma->setHypothetical(true)->setHypothetical(false)->isHypothetical());
    }
}
