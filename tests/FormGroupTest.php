<?php

/** @noinspection PhpLanguageLevelInspection */

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\Collections\FormNodes;
use Zalmoksis\Dictionary\Model\{Form, FormGroup, FormLabel, Node};

class FormGroupTest extends TestCase {

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, new FormGroup());
    }

    function testNodeName(): void {
        $this->assertEquals('form group', FormGroup::NODE_NAME);
    }

    function testFormLabel(): void {
        $this->assertNull((new Form())->getFormLabel());

        $form = new Form(formLabel: new FormLabel('form label'));

        $this->assertEquals(
            new FormLabel('form label'),
            $form->getFormLabel()
        );
    }

    function testFormNodes(): void {
        $formGroup = new FormGroup(
            formNodes: new FormNodes(
                (new Form(new FormLabel('form label 1.1'))),
                (new Form(new FormLabel('form label 1.2'))),
            )
        );

        $this->assertEquals(
            new FormNodes(
                (new Form(new FormLabel('form label 1.1'))),
                (new Form(new FormLabel('form label 1.2'))),
            ),
            $formGroup->getFormNodes()
        );
    }
}
