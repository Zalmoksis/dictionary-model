<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Node, SoundChange, Value};

class SoundChangeTest extends TestCase {
    protected SoundChange $soundChange;

    function setUp(): void {
        $this->soundChange = new SoundChange('some sound change');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->soundChange);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->soundChange);
    }

    function testNodeName(): void {
        $this->assertEquals('sound change', $this->soundChange::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('some sound change', $this->soundChange->getValue());
    }
}
