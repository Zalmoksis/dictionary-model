<?php

/** @noinspection PhpLanguageLevelInspection */

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\Collections\{
    Domains,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    Translations,
    Varieties,
};
use Zalmoksis\Dictionary\Model\{
    Collocation,
    Definition,
    Domain,
    Headword,
    Node,
    Pronunciation,
    Register,
    Sense,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Interfaces\{
    NodeWithDefinition,
    NodeWithDomains,
    NodeWithHeadwords,
    NodeWithPronunciations,
    NodeWithRegisters,
    NodeWithSenses,
    NodeWithTranslations,
    NodeWithVarieties,
};

class CollocationTest extends TestCase {

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, new Collocation());
    }

    function testIfImplementsNodePartials(): void {
        $collocation = new Collocation();
        $this->assertInstanceOf(NodeWithHeadwords::class, $collocation);
        $this->assertInstanceOf(NodeWithPronunciations::class, $collocation);
        $this->assertInstanceOf(NodeWithVarieties::class, $collocation);
        $this->assertInstanceOf(NodeWithRegisters::class, $collocation);
        $this->assertInstanceOf(NodeWithDomains::class, $collocation);
        $this->assertInstanceOf(NodeWithDefinition::class, $collocation);
        $this->assertInstanceOf(NodeWithTranslations::class, $collocation);
        $this->assertInstanceOf(NodeWithSenses::class, $collocation);
    }

    function testNodeName(): void {
        $this->assertEquals('collocation', Collocation::NODE_NAME);
    }

    function testHeadwords(): void {
        $this->assertNull((new Collocation())->getHeadwords());

        $collocation = new Collocation(
            headwords: new Headwords(
                new Headword('headword 1'),
                new Headword('headword 2'),
            )
        );

        $this->assertEquals(
            new Headwords(
                new Headword('headword 1'),
                new Headword('headword 2'),
            ),
            $collocation->getHeadwords()
        );
    }

    function testPronunciations(): void {
        $this->assertNull((new Collocation())->getPronunciations());

        $collocation = new Collocation(
            pronunciations: new Pronunciations(
                new Pronunciation('pronunciation 1'),
                new Pronunciation('pronunciation 2')
            )
        );

        $this->assertEquals(
            new Pronunciations(
                new Pronunciation('pronunciation 1'),
                new Pronunciation('pronunciation 2'),
            ),
            $collocation->getPronunciations()
        );
    }

    function testVarieties(): void {
        $this->assertNull((new Collocation())->getVarieties());

        $collocation = new Collocation(
            varieties: new Varieties(
                new Variety('variety 1'),
                new Variety('variety 2'),
            )
        );

        $this->assertEquals(
            new Varieties(
                new Variety('variety 1'),
                new Variety('variety 2'),
            ),
            $collocation->getVarieties()
        );
    }

    function testRegisters(): void {
        $this->assertNull((new Collocation())->getRegisters());

        $collocation = new Collocation(
            registers: new Registers(
                new Register('register 1'),
                new Register('register 2'),
            )
        );

        $this->assertEquals(
            new Registers(
                new Register('register 1'),
                new Register('register 2'),
            ),
            $collocation->getRegisters()
        );
    }

    function testDomains(): void {
        $this->assertNull((new Collocation())->getDomains());

        $collocation = new Collocation(
            domains: new Domains(
                new Domain('domain 1'),
                new Domain('domain 2'),
            )
        );

        $this->assertEquals(
            new Domains(
                new Domain('domain 1'),
                new Domain('domain 2'),
            ),
            $collocation->getDomains()
        );
    }

    function testDefinition(): void {
        $this->assertNull((new Collocation())->getDefinition());

        $collocation = new Collocation(definition: new Definition('definition'));

        $this->assertEquals(
            new Definition('definition'),
            $collocation->getDefinition(),
        );
    }

    function testTranslations(): void {
        $this->assertNull((new Collocation())->getTranslations());

        $collocation = new Collocation(
            translations: new Translations(
                new Translation('translation 1'),
                new Translation('translation 2'),
            ));
        $this->assertEquals(
            new Translations(
                new Translation('translation 1'),
                new Translation('translation 2'),
            ),
            $collocation->getTranslations()
        );
    }

    function testSenses(): void {
        $this->assertNull((new Collocation())->getSenses());

        $collocation = new Collocation(
            senses: new Senses(
                new Sense(translations: new Translations(new Translation('translation 1'))),
                new Sense(translations: new Translations(new Translation('translation 2'))),
            )
        );
        $this->assertEquals(
            new Senses(
                new Sense(translations: new Translations(new Translation('translation 1'))),
                new Sense(translations: new Translations(new Translation('translation 2'))),
            ),
            $collocation->getSenses()
        );
    }
}
