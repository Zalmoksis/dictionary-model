<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Node, Pronunciation, Value};

class PronunciationTest extends TestCase {
    protected Pronunciation $pronunciation;

    function setUp(): void {
        $this->pronunciation = new Pronunciation('some pronunciation');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->pronunciation);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->pronunciation);
    }

    function testNodeName(): void {
        $this->assertEquals('pronunciation', $this->pronunciation::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('some pronunciation', $this->pronunciation->getValue());
    }
}
