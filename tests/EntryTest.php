<?php

/** @noinspection PhpLanguageLevelInspection */

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{
    Antonym,
    Category,
    Cognate,
    Collocation,
    Definition,
    Derivative,
    Domain,
    Entry,
    Etymon,
    Form,
    FormGroup,
    FormLabel,
    Gloss,
    Headword,
    HomographIndex,
    Language,
    Lemma,
    Node,
    Pronunciation,
    Register,
    Sense,
    SoundChange,
    Synonym,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Interfaces\{
    NodeWithAntonyms,
    NodeWithCategories,
    NodeWithCognates,
    NodeWithCollocations,
    NodeWithDefinition,
    NodeWithDerivatives,
    NodeWithDomains,
    NodeWithEtymons,
    NodeWithFormNodes,
    NodeWithHeadwords,
    NodeWithPronunciations,
    NodeWithRegisters,
    NodeWithSenses,
    NodeWithSoundChanges,
    NodeWithSynonyms,
    NodeWithTranslations,
    NodeWithVarieties,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Cognates,
    Collocations,
    Derivatives,
    Domains,
    Etymons,
    FormNodes,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    SoundChanges,
    Synonyms,
    Translations,
    Varieties,
};

class EntryTest extends TestCase {

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, new Entry());
    }

    function testIfImplementsNodePartials(): void {
        $entry = new Entry();
        $this->assertInstanceOf(NodeWithHeadwords::class, $entry);
        $this->assertInstanceOf(NodeWithPronunciations::class, $entry);
        $this->assertInstanceOf(NodeWithCategories::class, $entry);
        $this->assertInstanceOf(NodeWithFormNodes::class, $entry);
        $this->assertInstanceOf(NodeWithVarieties::class, $entry);
        $this->assertInstanceOf(NodeWithRegisters::class, $entry);
        $this->assertInstanceOf(NodeWithDomains::class, $entry);
        $this->assertInstanceOf(NodeWithDefinition::class, $entry);
        $this->assertInstanceOf(NodeWithTranslations::class, $entry);
        $this->assertInstanceOf(NodeWithSynonyms::class, $entry);
        $this->assertInstanceOf(NodeWithAntonyms::class, $entry);
        $this->assertInstanceOf(NodeWithCollocations::class, $entry);
        $this->assertInstanceOf(NodeWithSenses::class, $entry);
        $this->assertInstanceOf(NodeWithDerivatives::class, $entry);
        $this->assertInstanceOf(NodeWithEtymons::class, $entry);
        $this->assertInstanceOf(NodeWithSoundChanges::class, $entry);
        $this->assertInstanceOf(NodeWithCognates::class, $entry);
    }

    function testNodeName(): void {
        $this->assertEquals('entry', Entry::NODE_NAME);
    }

    function testHeadwords(): void {
        $this->assertNull((new Entry())->getHeadwords());

        $entry = new Entry(
            headwords: new Headwords(
                new Headword('headword 1'),
                new Headword('headword 2'),
            )
        );

        $this->assertEquals(
            new Headwords(
                new Headword('headword 1'),
                new Headword('headword 2'),
            ),
            $entry->getHeadwords()
        );
    }

    function testHomographIndex(): void {
        $this->assertNull((new Entry())->getHomographIndex());

        $homographIndex = new Entry(homographIndex: new HomographIndex(2));

        $this->assertEquals(
            new HomographIndex(2),
            $homographIndex->getHomographIndex()
        );
    }

    function testPronunciations(): void {
        $this->assertNull((new Entry())->getPronunciations());

        $entry = (new Entry(
            pronunciations: new Pronunciations(
                new Pronunciation('pronunciation 1'),
                new Pronunciation('pronunciation 2'),
            )
        ));

        $this->assertEquals(
            new Pronunciations(
                new Pronunciation('pronunciation 1'),
                new Pronunciation('pronunciation 2'),
            ),
            $entry->getPronunciations()
        );
    }

    function testCategories(): void {
        $this->assertNull((new Entry())->getCategories());

        $entry = new Entry(
            categories: new Categories(
                new Category('category 1'),
                new Category('category 2'),
            )
        );
        $this->assertEquals(
            new Categories(
                new Category('category 1'),
                new Category('category 2'),
            ),
            $entry->getCategories()
        );
    }

    function testFormNodes(): void {
        $this->assertNull((new Entry())->getFormNodes());

        $entry = new Entry(
            formNodes: new FormNodes(
                new Form(
                    formLabel: new FormLabel('label 1'),
                    headwords: new Headwords(new Headword('form 1'))
                ),
                (new FormGroup(
                    formLabel: new FormLabel('label 2'),
                    formNodes: new FormNodes(
                        (new Form(
                            formLabel: new FormLabel('label 2.1'),
                            headwords: new Headwords(new Headword('form 2.1')),
                        ))
                    )
                )),
            )
        );
        $this->assertEquals(
            new FormNodes(
                new Form(
                    formLabel: new FormLabel('label 1'),
                    headwords: new Headwords(new Headword('form 1')),
                ),
                (new FormGroup(
                    formLabel: new FormLabel('label 2'),
                    formNodes: new FormNodes(
                        (new Form(
                            formLabel: new FormLabel('label 2.1'),
                            headwords: new Headwords(new Headword('form 2.1')),
                        ))
                    )
                )),
            ),
            $entry->getFormNodes()
        );
    }

    function testVarieties(): void {
        $this->assertNull((new Entry())->getVarieties());

        $entry = new Entry(
            varieties: new Varieties(
                new Variety('variety 1'),
                new Variety('variety 2'),
            )
        );

        $this->assertEquals(
            new Varieties(
                new Variety('variety 1'),
                new Variety('variety 2'),
            ),
            $entry->getVarieties()
        );
    }

    function testRegisters(): void {
        $this->assertNull((new Entry())->getRegisters());

        $entry = new Entry(
            registers: new Registers(
                new Register('register 1'),
                new Register('register 2'),
            )
        );

        $this->assertEquals(
            new Registers(
                new Register('register 1'),
                new Register('register 2'),
            ),
            $entry->getRegisters()
        );
    }

    function testDomains(): void {
        $this->assertNull((new Entry())->getDomains());

        $entry = new Entry(
            domains: new Domains(
                new Domain('domain 1'),
                new Domain('domain 2'),
            )
        );

        $this->assertEquals(
            new Domains(
                new Domain('domain 1'),
                new Domain('domain 2'),
            ),
            $entry->getDomains()
        );
    }

    function testDefinition(): void {
        $this->assertNull((new Entry())->getDefinition());

        $entry = new Entry(definition: new Definition('definition'));

        $this->assertEquals(
            new Definition('definition'),
            $entry->getDefinition(),
        );
    }

    function testTranslations(): void {
        $this->assertNull((new Entry())->getTranslations());

        $entry = new Entry(
            translations: new Translations(
                new Translation('translation 1'),
                new Translation('translation 2'),
            )
        );

        $this->assertEquals(
            new Translations(
                new Translation('translation 1'),
                new Translation('translation 2'),
            ),
            $entry->getTranslations()
        );
    }

    function testSynonyms(): void {
        $this->assertNull((new Entry())->getSynonyms());

        $entry = new Entry(
            synonyms: new Synonyms(
                new Synonym('synonym 1'),
                new Synonym('synonym 2'),
            )
        );

        $this->assertEquals(
            new Synonyms(
                new Synonym('synonym 1'),
                new Synonym('synonym 2'),
            ),
            $entry->getSynonyms()
        );
    }

    function testAntonyms(): void {
        $this->assertNull((new Entry())->getAntonyms());

        $entry = new Entry(
            antonyms: new Antonyms(
                new Antonym('antonym 1'),
                new Antonym('antonym 2'),
        ));

        $this->assertEquals(
            new Antonyms(
                new Antonym('antonym 1'),
                new Antonym('antonym 2'),
            ),
            $entry->getAntonyms()
        );
    }

    function testCollocations(): void {
        $this->assertNull((new Entry())->getCollocations());

        $entry = new Entry(
            collocations: new Collocations(
                new Collocation(
                    headwords: new Headwords(new Headword('headword 1')),
                    translations: new Translations(new Translation('translation 1')),
                ),
                new Collocation(
                    headwords: new Headwords(new Headword('headword 2')),
                    translations: new Translations(new Translation('translation 2')),
                ),
            )
        );

        $this->assertEquals(
            new Collocations(
                new Collocation(
                    headwords: new Headwords(new Headword('headword 1')),
                    translations: new Translations(new Translation('translation 1')),
                ),
                new Collocation(
                    headwords: new Headwords(new Headword('headword 2')),
                    translations: new Translations(new Translation('translation 2')),
                ),
            ),
            $entry->getCollocations()
        );
    }

    function testSenses(): void {
        $this->assertNull((new Entry())->getSenses());

        $entry = new Entry(
            senses: new Senses(
                new Sense(translations: new Translations(new Translation('translation 1'))),
                new Sense(translations: new Translations(new Translation('translation 2'))),
            )
        );

        $this->assertEquals(
            new Senses(
                new Sense(translations: new Translations(new Translation('translation 1'))),
                new Sense(translations: new Translations(new Translation('translation 2'))),
            ),
            $entry->getSenses()
        );
    }

    function testDerivatives(): void {
        $this->assertNull((new Entry())->getDerivatives());

        $entry = new Entry(
            derivatives: new Derivatives(
                new Derivative('derivative 1'),
                new Derivative('derivative 2'),
            )
        );

        $this->assertEquals(
            new Derivatives(
                new Derivative('derivative 1'),
                new Derivative('derivative 2'),
            ),
            $entry->getDerivatives()
        );
    }

    function testEtymons(): void {
        $this->assertNull((new Entry())->getEtymons());

        $entry = new Entry(
            etymons: new Etymons(
                new Etymon(
                    language: new Language('language 1'),
                    lemma: new Lemma('lemma 1'),
                    gloss: new Gloss('gloss 1'),
                ),
                new Etymon(
                    language: new Language('language 2'),
                    lemma: new Lemma('lemma 2'),
                    gloss: new Gloss('gloss 2'),
                ),
            )
        );

        $this->assertEquals(
            new Etymons(
                new Etymon(
                    language: new Language('language 1'),
                    lemma: new Lemma('lemma 1'),
                    gloss: new Gloss('gloss 1'),
                ),
                new Etymon(
                    language: new Language('language 2'),
                    lemma: new Lemma('lemma 2'),
                    gloss: new Gloss('gloss 2'),
                ),
            ),
            $entry->getEtymons()
        );
    }

    function testSoundChanges(): void {
        $this->assertNull((new Entry())->getSoundChanges());

        $entry = new Entry(
            soundChanges: new SoundChanges(
                new SoundChange('sound change 1'),
                new SoundChange('sound change 2'),
            )
        );

        $this->assertEquals(
            new SoundChanges(
                new SoundChange('sound change 1'),
                new SoundChange('sound change 2'),
            ),
            $entry->getSoundChanges()
        );
    }

    function testCognates(): void {
        $this->assertNull((new Entry())->getCognates());

        $entry = new Entry(
            cognates: new Cognates(
                new Cognate(
                    language: new Language('language 1'),
                    lemma: new Lemma('lemma 1'),
                    gloss: new Gloss('gloss 1'),
                ),
                new Cognate(
                    language: new Language('language 2'),
                    lemma: new Lemma('lemma 2'),
                    gloss: new Gloss('gloss 2'),
                ),
            )
        );

        $this->assertEquals(
            new Cognates(
                new Cognate(
                    language: new Language('language 1'),
                    lemma: new Lemma('lemma 1'),
                    gloss: new Gloss('gloss 1'),
                ),
                new Cognate(
                    language: new Language('language 2'),
                    lemma: new Lemma('lemma 2'),
                    gloss: new Gloss('gloss 2'),
                ),
            ),
            $entry->getCognates()
        );
    }
}
