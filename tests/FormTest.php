<?php

/** @noinspection PhpLanguageLevelInspection */

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\Collections\Headwords;
use Zalmoksis\Dictionary\Model\{Form, FormLabel, Headword, Node};

class FormTest extends TestCase {

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, new Form());
    }

    function testNodeName(): void {
        $this->assertEquals('form', Form::NODE_NAME);
    }

    function testFormLabel(): void {
        $this->assertNull((new Form())->getFormLabel());

        $form = new Form(formLabel: new FormLabel('form label'));

        $this->assertEquals(
            new FormLabel('form label'),
            $form->getFormLabel()
        );
    }

    function testHeadwords(): void {
        $this->assertNull((new Form())->getHeadwords());

        $form = new Form(
            headwords: new Headwords(
                new Headword('headword 1'),
                new Headword('headword 2')
            )
        );

        $this->assertEquals(
            new Headwords(
                new Headword('headword 1'),
                new Headword('headword 2')
            ),
            $form->getHeadwords()
        );
    }
}
