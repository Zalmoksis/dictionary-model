<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Definition, Node, Value};

class DefinitionTest extends TestCase {
    protected Definition $definition;

    function setUp(): void {
        $this->definition = new Definition('some definition');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->definition);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->definition);
    }

    function testNodeName(): void {
        $this->assertEquals('definition', $this->definition::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('some definition', $this->definition->getValue());
    }
}
