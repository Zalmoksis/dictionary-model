<?php

/** @noinspection PhpLanguageLevelInspection */

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{
    Cognate,
    Gloss,
    Language,
    Lemma,
    Node,
};

class CognateTest extends TestCase {

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, new Cognate());
    }

    function testNodeName(): void {
        $this->assertEquals('cognate', Cognate::NODE_NAME);
    }

    function testLanguage(): void {
        $this->assertNull((new Cognate())->getLanguage());

        $cognate = new Cognate(language: new Language('some language'));

        $this->assertEquals(
            new Language('some language'),
            $cognate->getLanguage());
    }

    function testLemma(): void {
        $this->assertNull((new Cognate())->getLemma());

        $cognate = new Cognate(lemma: new Lemma('some lemma'));

        $this->assertEquals(
            new Lemma('some lemma'),
            $cognate->getLemma()
        );
    }

    function testGloss(): void {
        $this->assertNull((new Cognate())->getGloss());

        $cognate = new Cognate(gloss: new Gloss('some gloss'));

        $this->assertEquals(
            new Gloss('some gloss'),
            $cognate->getGloss()
        );
    }
}
