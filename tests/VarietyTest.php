<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Node, Value, Variety};

class VarietyTest extends TestCase {
    protected Variety $variety;

    function setUp(): void {
        $this->variety = new Variety('some variety');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->variety);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->variety);
    }

    function testNodeName(): void {
        $this->assertEquals('variety', $this->variety::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('some variety', $this->variety->getValue());
    }
}
