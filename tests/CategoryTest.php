<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Model\Tests;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{
    Category,
    Node,
    Value,
};

class CategoryTest extends TestCase {
    protected Category $category;

    function setUp(): void {
        $this->category = new Category('some category');
    }

    function testIfImplementsValue(): void {
        $this->assertInstanceOf(Value::class, $this->category);
    }

    function testIfImplementsNode(): void {
        $this->assertInstanceOf(Node::class, $this->category);
    }

    function testNodeName(): void {
        $this->assertEquals('category', $this->category::NODE_NAME);
    }

    function testValue(): void {
        $this->assertEquals('some category', $this->category->getValue());
    }
}
